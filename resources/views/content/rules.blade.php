@extends('layouts.main')

@section('content')
    <div class="hero-wrap js-fullheight auto-height "
         style="background-image: url('{{URL::asset("images/bg_grey.v4.png")}}');">
        <div class="overlay"></div>
        <div class="container mycontainer-second result-page App">
            <h4 class="title2"> Szabályzat </h4>
            <p class=" text-secondary my-custom-text ">
                Kedves jelentkező, e-sportoló. Köszöntünk a Fifahungary e-sport FIFA bajnokságain. Ahhoz, hogy a játék
                mindenki számára élvezhető legyen, szükség van néhány szabály betartására, amely szabályokokat lentebb
                találjátok! <br/>
                <br/>
                <span class="rules-title">1. Mivel a FUT játékmódra alapozunk, ezért a játékosoknak a saját FUT csapataikat kell használniuk a meccsek lejátszására.</span>
                <span class="rules-subtitle important">a.) LOAN játékosok használata nem megengedett! (indoklás: Friendly módban nem csökken a LOAN játékosok szerződése, így fenáll az esély hogy valaki csak LOAN, magas értékelésű játékosokból álló csapatot használjon) </span>
                <span class="rules-title">2. Játékmenet: normál FUT Friendly meccs! </span>
                <span class="rules-subtitle "> a.) A meccsek oda-visszavágó típusuak. Kivéve a döntő.</span>
                <span class="rules-subtitle "> b.) Nincs idegenben lőtt gólszabály!</span>
                <span class="rules-subtitle "> c.) Ha az első meccsen az eredmény döntetlen, akkor azt kell leadni. </span>
                <span class="rules-subtitle "> d.) Ha a második meccsen is döntetlen az eredmény, akkor hosszabítás következik. Ha eldől a meccs, akkor a hosszabítás végén kialakult eredmény kell leadni. Ha 11-es párbajra kerül sor, akkor a győztes csapat góljaihoz +1-et hozzákell adni, és úgy leadni az eredményt.  </span>
                <span class="rules-subtitle "> e.) Ha a két lejátszott meccs közül egyiksem döntetlen, viszont a két meccs összesített pontszáma döntetlent eredményez, akkor egy 3-ik meccs lejátszására van szükség. Hozzá kell adni +1 gólt a 3.-ik meccset megnyerő játékos góljaihoz, és úgy leadni az eredmény. </span>
                <span class="rules-subtitle  rules-expl"> Példa: <br/> 1.meccs: Barcelona 1-2 Chelsea <br/> 2.meccs: Chelsea 1-2 Barcelona <br/> Összesített eredmény: Barcelona 3-3 Chelsea <br/> Harmadik meccs győztes: Chelsea (ha szükséges hosszabítás és 11-es párbaj is) <br/> A második meccs modosított eredménye: Chelsea 2-2 Barcelona </span> </span>

                <span class="rules-title">3. Díjazások: </span>
                <span class="rules-subtitle "> a.) Ingyenes nevezéses bajnokság típusok: ezeknél a bajnokságoknál nincs nevezési díj, így nyeremények sem kerülnek kiosztásra.  </span>
                <span class="rules-subtitle "> b.) Nevezési díjas bajnokság típusok: FIFA PONT, valamint FIFA COIN nyeremények.  A nevezési díjat Revolut-on, valamint PayPal-en keresztül lehet befizetni. </span>
                <span class="rules-subtitle "> c.) Támogatott bajnokság típusok: Ezeknél a bajnokságoknál a nevezés ingyenes, viszont nyeremények kerülnek kiosztásra FIFA PONT és FIFA Coin formájában. A nevezés ingyenes, viszont kritiériumokhoz kötött (például: Részt kellet venned a Futhungary által szervezett utolsó 6 bajnokságból, legalább 4-en). Ilyen bajnokság típusok megközelítőleg 1-2 havonta kerülnek megszervezésre.  </span>

                <span class="rules-title">4. Nevezési díjak: </span>
                <span class="rules-subtitle">a.) A nevési díjakat REVOLUT-on illetve PayPal-en keresztül van lehetőség befizetni!</span>


            </p>

        </div>
    </div>
@endsection
