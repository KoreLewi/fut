@extends('layouts.main')

@section('content')
    <div class="hero-wrap js-fullheight" style="background-image: url('{{URL::asset("images/fut20_bg1.jpg")}}');"
         data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start"
                 data-scrollax-parent="true">
                <div class="col-md-6 mt-5 pt-5 ftco-animate mt-5" data-scrollax=" properties: { translateY: '70%' }">
                    <h1 class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"> FUT 20 bajnokság.
                        Nyereményekkel!</h1>
                    <p class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"> Jelentkezz Te is
                        hamarosan induló FUT 20 bajnokságunkra. Ha még nem regisztráltál, kattins a regisztráció
                        gombra.</p>
                    <p><a href="/tournament-registration-show-2" class="btn btn-secondary py-3 px-4"> Jelentkezés a
                            bajnokságra </a>
                        <a href="/register" class="btn btn-primary py-3 px-4"> Regisztráció </a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="hero-wrap js-fullheight auto-height "
         style="background-image: url('{{URL::asset("images/BIRZKpS.png")}}');">
        <div class="overlay"></div>
        <div class="container mycontainer-second result-page App">
            <p class="my-custom-text-white text-white"> Elindult a Futhungary FIFA e-sport weboldala. A bajnokságra jelentkezni csak regisztráció után lehetséges. <br/>
                Figyelem: a regisztrációkor szükséges megadni a platformot is!
                <br/> <br/>
                Amennyiben nem szeretnél lemaradni a legfrissebb hírekről, bajnokságokról, lájkold a  <a href="https://www.facebook.com/FuthungaryTournaments"  target="_blank"> Facebook oldalunkat </a>, és lépj be <a href="https://www.facebook.com/groups/512731792766628/?source_id=106831347589269" target="_blank">Facebook csoportunkba </a> .
                <br/> <br/>
                Ne felejtsd el elolvasni a <a href="/rules"> szabályzatot!</a>
            </p>
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", function () {
            $('#whiteheader').addClass('whiteheader');
        });
    </script>
@endsection
