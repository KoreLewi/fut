<!DOCTYPE html>
<html lang="hu">

<!-- head -->
<head>
    @include('commons.head')
</head>
<!-- end head -->

<body>
<div id="header">
    {{ App\Http\Controllers\HeaderController::headerMenu() }}

{{--    @include('commons.header')--}}
</div>
<!-- wrapper -->
<div id="wrapper">
    @yield('content')
</div>
<!-- end wrapper -->

<!-- footer -->
@include('commons.footer')
<!-- end footer -->
</body>

</html>
