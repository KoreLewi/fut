@extends('layouts.main')

@section('content')
    <div class="hero-wrap js-fullheight auto-height "
         style="background-image: url('{{URL::asset("images/bg_grey.v4.png")}}');">
        <div class="overlay"></div>
        <div class="container mycontainer-second result-page App">
            <div class="row">
                <div class="col-lg-8 col-md-8  col-sm-12">
                    <h4 class="title2"> Függőben lévő meccseid </h4>
                    <div class="mathes-holder">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if (session('msg'))
                            <div class="alert alert-succes">
                                {{ session('msg') }}
                            </div>
                        @endif
                        @foreach($participantinChamps as $champ)
                            @if($results[$champs[$champ]['id']])

                                <h3 class="title3 champ-title text-capitalize">
                                    Esemény: {{$champs[$champ]['name']}} </h3>

                                @foreach($results[$champs[$champ]['id']] as $result)
                                    <form class="result-form" action="/accept-result" method="get">
                                <span class="result-inside-holder">
                                    <label
                                            for="homegoal"> {{$clubsInChamp[$champs[$champ]['id']][$result['home_club_id']]['name']}}  </label>
                                    <input type="text" id="home_goals" name="homegoal" class="score" disabled="disabled"
                                           value="{{$result['home_goals']}}">
                                    <span class="result-line"> - </span>
                                    <input type="text" id="away_goal" name="awaygoal" class="score" disabled="disabled"
                                           value="{{$result['away_goals']}}">
                                    <label
                                            for="awaygoal">  {{$clubsInChamp[$champs[$champ]['id']][$result['away_club_id']]['name']}} </label>
                                </span>
                                        <input type="hidden" value="{{$result['id']}}" name="result_id">
                                        <input type="submit" class=" button btn send-result-btn btn-success"
                                               value="Elfogadás">
                                    </form>
                                @endforeach
                            @endif

                        @endforeach
                    </div>
                </div>
                <div class="col-lg-4 col-md-4  col-sm-12">
                    <p class="title2 h5"> Profil Adatok</p>
                    <p class="name text-info lead"> Felhasználónév: {{$user->name}}</p>
                    <p class="name text-info lead"> Email cím: {{$user->email}}</p>

                    <p class="title2 h5"> Folyamatban lévő bajnokságok</p>
                    @foreach($champs as $champ)
                        @if($champ['status'] == 'In progress')
                            <a class="link" href="/championship-{{$champ['id']}}">
                                <p class="name lead text-primary link"> {{$champ['name']}} </p>
                            </a>
                        @endif
                    @endforeach
                </div>
            </div>

        </div>
    </div>
@endsection

<script>
    document.addEventListener("DOMContentLoaded", function () {
        $(".result-form").on("submit", function () {
            return confirm("Biztos vagy benne?");
        });
    });
</script>