<title> FutHungary - Magyar FIFA ULTIMATE TEAM </title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="shortcut icon" href="{{URL::asset('images/favicon.ico')}}" />


<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700&display=swap" rel="stylesheet">

<link rel="stylesheet" href="{{ URL::asset('css/normalize.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/open-iconic-bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/animate.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/owl.carousel.min.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/owl.theme.default.min.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/aos.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/ionicons.min.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-datepicker.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/jquery.timepicker.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/flaticon.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/icomoon.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/main.css') }}" />



