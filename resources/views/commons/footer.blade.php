
<footer class="ftco-footer ftco-section">
    <div class="container-fluid px-md-5">
                <div class="ftco-footer-widget ">
                    <h2 class="ftco-heading-2"> FUTHUNGARY</h2>
                    <p>A férfiaknak nincsenek céljaik. Így aztán kitalálnak párat, és felállítják azokat egy focipálya két végében.</p>
                    <ul class="ftco-footer-social list-unstyled">
{{--                        <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>--}}
                        <li class="ftco-animate"><a href="https://www.facebook.com/FuthungaryTournaments" target="_blank"><span class="icon-facebook"></span></a></li>
{{--                        <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>--}}
                    </ul>
                </div>

        <div class="row">
            <div class="col-md-12 text-center">

                <p>
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Korel
                   </p>
            </div>
        </div>
    </div>
</footer>


<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ URL::asset('js/popper.min.js') }}"></script>

<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>

<script src="{{ URL::asset('js/jquery.easing.1.3.js') }}"></script>

<script src="{{ URL::asset('js/jquery.waypoints.min.js') }}"></script>

<script src="{{ URL::asset('js/jquery.stellar.min.js') }}"></script>

<script src="{{ URL::asset('js/owl.carousel.min.js') }}"></script>

<script src="{{ URL::asset('js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ URL::asset('js/aos.js') }}"></script>

<script src="{{ URL::asset('js/jquery.animateNumber.min.js') }}"></script>

<script src="{{ URL::asset('js/bootstrap-datepicker.js') }}"></script>

<script src="{{ URL::asset('js/scrollax.min.js') }}"></script>
<script src="{{ URL::asset('js/main.js') }}"></script>

