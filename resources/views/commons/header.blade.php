<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="/"><img class="img-fluid logo" src="{{URL::asset("images/logo.png")}}"> <span> FutHungary</span></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto" id="whiteheader">

                @auth
                    @if( $activChampId)
                        <li class="nav-item"><a href="/championship-{{$activChampId }}" class="nav-link">{{$championship->name}}</a></li>

                        <li class="nav-item"><a href="/my-matches-{{$activChampId}}"
                                                class="nav-link"> {{$championship->name}} eredmény leadás</a>
                        </li>
                    @endif

                @endauth
                <li class="nav-item"><a href="/rules" class="nav-link">Szabályzat</a></li>
                {{--                <li class="nav-item cta"><a href="#" class="nav-link">Buy Ticket</a></li>--}}
                @auth
                    <li class="nav-item">
                        <a href="/my-account" class="nav-link defaultcase tooltip">
                            Üdv, {{{ Auth::user()->name }}}
                            <span class="tooltiptext"> Kattints a profilodhoz</span>
                        </a>
                    </li>
                    <li class="nav-item "><a href="/logout" class="nav-link defaultcase">Kijelentkezés</a></li>

                @endauth
                @guest
                    <li class="nav-item"><a href="/login" class="nav-link">Bejelentkezés</a></li>
                    <li class="nav-item"><a href="/register" class="nav-link">Regisztráció</a></li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
