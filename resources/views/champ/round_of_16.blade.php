@extends('layouts.main')

@section('content')
    <div class="hero-wrap js-fullheight auto-height"
         style="background-image: url('{{URL::asset("images/bg_grey.v4.png")}}');">
        <div class="overlay"></div>
        <div class="container mycontainer-second App result-page-container">
            <h1 class="title"> {{$champ->name}}</h1>
            <main id="tournament" class="club64">

                {{--                RoundOf16--}}
                <ul class="round round-3">
                    <li class="spacer">&nbsp;</li>
                    @foreach($matches['RoundOf16'] as $match)
                        <li class="game game-top @if(!isset($clubs[$match['home_club_id']]['name'])) nonvisible @endif ">
                            @if(isset($clubs[$match['home_club_id']]['name']))
                                <span
                                        class="club-name  @if( isset($match['winner_club_id']) && $match['home_club_id'] != $match['winner_club_id'] ) eliminated @endif"> {{$clubs[$match['home_club_id']]['name']}}</span>
                                <span class="total-match score">{{$match['total_home_goals']}}</span>
                                <span class="second-match score"> {{$match['second_match_home_goals']}}</span>
                                <span class="first-match score"> {{$match['first_match_home_goals']}}</span>
                            @else
                                <span class="club-name">  TEST </span>
                                <span class="total-match score"> </span>
                                <span class="second-match score">0 </span>
                                <span class="first-match score">0 </span>
                            @endif

                        </li>
                        <li class="game game-spacer">&nbsp;</li>
                        <li class="game game-bottom @if(!isset($clubs[$match['away_club_id']]['name'])) nonvisible @endif ">
                            @if(isset($clubs[$match['away_club_id']]['name']))
                                <span
                                        class="club-name  @if( isset($match['winner_club_id']) && $match['away_club_id'] != $match['winner_club_id'] ) eliminated @endif">{{$clubs[$match['away_club_id']]['name']}} </span>
                                <span class="total-match score">{{$match['total_away_goals']}}</span>
                                <span class="second-match score"> {{$match['second_match_away_goals']}}</span>
                                <span class="first-match score"> {{$match['first_match_away_goals']}}</span>
                            @else
                                <span class="club-name">  TEST </span>
                                <span class="total-match score"> 0</span>
                                <span class="second-match score">0 </span>
                                <span class="first-match score">0 </span>
                            @endif

                        </li>
                        <li class="spacer">&nbsp;</li>
                    @endforeach
                </ul>

                {{--                Quarterfinals--}}
                <ul class="round round-4">
                    <li class="spacer">&nbsp;</li>
                    @foreach($matches['Quarterfinals'] as $match)
                        <li class="game game-top @if(!isset($clubs[$match['home_club_id']]['name'])) nonvisible @endif ">
                            @if(isset($clubs[$match['home_club_id']]['name']))
                                <span
                                        class="club-name  @if( isset($match['winner_club_id']) && $match['home_club_id'] != $match['winner_club_id'] ) eliminated @endif"> {{$clubs[$match['home_club_id']]['name']}}</span>
                                <span class="total-match score">{{$match['total_home_goals']}}</span>
                                <span class="second-match score"> {{$match['second_match_home_goals']}}</span>
                                <span class="first-match score"> {{$match['first_match_home_goals']}}</span>
                            @else
                                <span class="club-name">  TEST </span>
                                <span class="total-match score"> </span>
                                <span class="second-match score">0 </span>
                                <span class="first-match score">0 </span>
                            @endif

                        </li>
                        <li class="game game-spacer">&nbsp;</li>
                        <li class="game game-bottom @if(!isset($clubs[$match['away_club_id']]['name'])) nonvisible @endif ">
                            @if(isset($clubs[$match['away_club_id']]['name']))
                                <span
                                        class="club-name  @if( isset($match['winner_club_id']) && $match['away_club_id'] != $match['winner_club_id'] ) eliminated @endif">{{$clubs[$match['away_club_id']]['name']}} </span>
                                <span class="total-match score">{{$match['total_away_goals']}}</span>
                                <span class="second-match score"> {{$match['second_match_away_goals']}}</span>
                                <span class="first-match score"> {{$match['first_match_away_goals']}}</span>
                            @else
                                <span class="club-name">  TEST </span>
                                <span class="total-match score"> 0</span>
                                <span class="second-match score">0 </span>
                                <span class="first-match score">0 </span>
                            @endif

                        </li>
                        <li class="spacer">&nbsp;</li>
                    @endforeach
                </ul>

                {{--                SemiFinals --}}
                <ul class="round round-5">
                    <li class="spacer">&nbsp;</li>
                    @foreach($matches['Semifinals'] as $match)
                        <li class="game game-top @if(!isset($clubs[$match['home_club_id']]['name'])) nonvisible @endif ">
                            @if(isset($clubs[$match['home_club_id']]['name']))
                                <span
                                        class="club-name  @if( isset($match['winner_club_id']) && $match['home_club_id'] != $match['winner_club_id'] ) eliminated @endif"> {{$clubs[$match['home_club_id']]['name']}}</span>
                                <span class="total-match score">{{$match['total_home_goals']}}</span>
                                <span class="second-match score"> {{$match['second_match_home_goals']}}</span>
                                <span class="first-match score"> {{$match['first_match_home_goals']}}</span>
                            @else
                                <span class="club-name">  TEST </span>
                                <span class="total-match score"> </span>
                                <span class="second-match score">0 </span>
                                <span class="first-match score">0 </span>
                            @endif

                        </li>
                        <li class="game game-spacer">&nbsp;</li>
                        <li class="game game-bottom @if(!isset($clubs[$match['away_club_id']]['name'])) nonvisible @endif ">
                            @if(isset($clubs[$match['away_club_id']]['name']))
                                <span
                                        class="club-name  @if( isset($match['winner_club_id']) && $match['away_club_id'] != $match['winner_club_id'] ) eliminated @endif">{{$clubs[$match['away_club_id']]['name']}} </span>
                                <span class="total-match score">{{$match['total_away_goals']}}</span>
                                <span class="second-match score"> {{$match['second_match_away_goals']}}</span>
                                <span class="first-match score"> {{$match['first_match_away_goals']}}</span>
                            @else
                                <span class="club-name">  TEST </span>
                                <span class="total-match score"> 0</span>
                                <span class="second-match score">0 </span>
                                <span class="first-match score">0 </span>
                            @endif

                        </li>
                        <li class="spacer">&nbsp;</li>
                    @endforeach
                </ul>

                {{--                Final--}}
                <ul class="round round-6">
                    <li class="spacer">&nbsp;</li>

                    <li class="game game-top  @if( !isset($clubs[$matches['Finals'][0]['home_club_id']]['name']) ) nonvisible @endif">
                        @if(isset($clubs[$matches['Finals'][0]['home_club_id']]['name']))
                            <span
                                    class="club-name @if( isset($matches['Finals'][0]['winner_club_id']) && $matches['Finals'][0]['home_club_id'] != $matches['Finals'][0]['winner_club_id'] ) eliminated @endif"> {{$clubs[$matches['Finals'][0]['home_club_id']]['name']}} </span>
                            <span class="total-match score">{{$matches['Finals'][0]['first_match_home_goals']}}</span>
                            <span class="second-match score"> </span>
                            <span class="first-match score"> </span>
                        @else
                            <span class="club-name">  TEST </span>
                            <span class="total-match score"> 0</span>
                            <span class="second-match score">0 </span>
                            <span class="first-match score">0 </span>
                        @endif
                    </li>
                    <li class="game game-spacer">&nbsp;</li>
                    <li class="game game-bottom  @if( !isset($clubs[$matches['Finals'][0]['away_club_id']]['name']) ) nonvisible @endif">
                        @if(isset($clubs[$matches['Finals'][0]['away_club_id']]['name']))
                            <span
                                    class="club-name @if( isset($matches['Finals'][0]['winner_club_id']) && $matches['Finals'][0]['away_club_id'] != $matches['Finals'][0]['winner_club_id'] ) eliminated @endif"> {{$clubs[$matches['Finals'][0]['away_club_id']]['name']}} </span>
                            <span class="total-match score">{{$matches['Finals'][0]['first_match_away_goals']}}</span>
                            <span class="second-match score"> </span>
                            <span class="first-match score"> </span>
                        @else
                            <span class="club-name">  TEST </span>
                            <span class="total-match score"> 0</span>
                            <span class="second-match score">0 </span>
                            <span class="first-match score">0 </span>
                        @endif
                    </li>


                    <li class="spacer">&nbsp;</li>
                </ul>

                {{--                Winner--}}
                <ul class="round round-7">
                    <li class="spacer">&nbsp;</li>

                    <li class="game game-top winner-winner-winner @if( !isset($clubs[$matches['Finals'][0]['winner_club_id']]['name'])) nonvisible @endif ">
                        <span
                                class="club-name winner-club  ">
                            @if( isset($clubs[$matches['Finals'][0]['winner_club_id']]['name']))
                                {{$clubs[$matches['Finals'][0]['winner_club_id']]['name']}} </span>
                        @endif
                    </li>

                    <li class="spacer">&nbsp;</li>
                </ul>
            </main>
        </div>
    </div>
@endsection
