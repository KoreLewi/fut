@extends('layouts.main')

@section('content')
    <div class="hero-wrap js-fullheight auto-height"
         style="background-image: url('{{URL::asset("images/bg_grey.v4.png")}}');">
        <div class="overlay"></div>
        <div class="container mycontainer-second App result-page ">

            @if (isset($error))
                <div class="alert alert-danger">
                    {{ $error}}
                </div>
            @elseif( isset($error_already) )
                <div class="alert alert-info">
                    {{ $error_already}}: {{$champ->name}}
                </div>
            @else
                <h3 class="title2 font-weight-normal">Regisztrációs űrlap a(z) {{$champ->name}} eseményre </h3>

                <form class="result-form" action="/tournament-registration" method="get">
                <span class="result-inside-holder">
                    <fieldset>
                    <label for="champname" class="col-form-label label"> Esemény név </label> <br/>
                    <input type="text" disabled="disabled" id="champ_name" name="champname"
                           class="input-append custom-input"
                           value="{{$champ['name']}}">
                    </fieldset>
                    <input type="hidden" name="champ_id" value="{{$champ['id']}}">
                </span>
                    <fieldset>
                        <br/>
                        <input type="submit" class="btn-info button btn send-result-btn" value="Regisztráció">
                    </fieldset>
                </form>
            @endif

        </div>
    </div>
@endsection
