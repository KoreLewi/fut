@extends('layouts.main')

@section('content')
    <div class="hero-wrap js-fullheight auto-height"
         style="background-image: url('{{URL::asset("images/BIRZKps.png")}}');">
        <div class="overlay"></div>
        <div class="container mycontainer-second App">
            <h1 class="title"> {{$champ->name}} </h1>
            <section class="Draw"><h2>Draw</h2>
                <ul>
                    <li>
                        <ul>
                            <li><span>Game 1</span>
                                <div>tes</div>
                                <div>SD</div>
                            </li>
                            <li><span>Game 2</span>
                                <div>D</div>
                                <div>S4R</div>
                            </li>
                            <li><span>Game 3</span>
                                <div>D5S</div>
                                <div>D</div>
                            </li>
                            <li><span>Game 4</span>
                                <div>SDS</div>
                                <div>dsd</div>
                            </li>
                            <li><span>Game 5</span>
                                <div>DS54</div>
                                <div>sad</div>
                            </li>
                            <li><span>Game 6</span>
                                <div>testes</div>
                                <div>SD4S</div>
                            </li>
                            <li><span>Game 7</span>
                                <div>DS</div>
                                <div>SD</div>
                            </li>
                            <li><span>Game 8</span>
                                <div>SR</div>
                                <div>SD</div>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <ul>
                            <li><span>Game 9</span>
                                <div>&nbsp;</div>
                                <div>&nbsp;</div>
                            </li>
                            <li><span>Game 10</span>
                                <div>&nbsp;</div>
                                <div>&nbsp;</div>
                            </li>
                            <li><span>Game 11</span>
                                <div>&nbsp;</div>
                                <div>&nbsp;</div>
                            </li>
                            <li><span>Game 12</span>
                                <div>&nbsp;</div>
                                <div>&nbsp;</div>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <ul>
                            <li><span>Game 13</span>
                                <div>&nbsp;</div>
                                <div>&nbsp;</div>
                            </li>
                            <li><span>Game 14</span>
                                <div>&nbsp;</div>
                                <div>&nbsp;</div>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <ul>
                            <li><span>Final</span>
                                <div>&nbsp;</div>
                                <div>&nbsp;</div>
                            </li>
                        </ul>
                    </li>
                </ul>
                <p>16 participants will play 4 round(s).</p></section>
        </div>
    </div>
@endsection
