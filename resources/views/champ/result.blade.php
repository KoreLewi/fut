@extends('layouts.main')

@section('content')
    <div class="hero-wrap js-fullheight auto-height "
         style="background-image: url('{{URL::asset("images/bg_grey.v4.png")}}');">
        <div class="overlay"></div>
        <div class="container mycontainer-second result-page App ">
            @if( isset($error) )
                <br/>
                <br/>
                <a href="https://www.facebook.com/pg/FuthungaryTournaments/" target="_blank">
                <p class=" alert alert-info"> Jelenleg nincs folyamatban lévő bajnokság, amelyre regisztráltál.
                    Amennyiben nem szeretnél lemaradni a következő bajnokságról, <span style="color:red"> KATTINTS IDE</span> és kövesd a FB-oldalunkat, hogy mindig
                    naprakész legyél!  </p>
                </a>
            @else
                <h2 class="title2">{{$user->name}} felhasználó meccsei, a(z) {{$champ->name}}-on belül </h2>
                <div class="mathes-holder">
                    @foreach($matches as $match)
                        <h4 class="title3">Meccs típus: {{$match->type}} </h4>
                        @if($match->hasRematch)
                            @if(!$match->isPlayedFirst)
                                @if( (isset($results[$match->id][1]) && ($results[$match->id][1]->match_nr == 1)) || (isset($results[$match->id][1]) && ($results[$match->id][1]->match_nr == 1)) )
                                    <form class="result-form" action="/save-result" method="get">
                                <span class="result-inside-holder">
                                    <span class="title3 matchnr"> Első meccs </span>
                                    <label for="homegoal"> {{$clubs[$match->home_club_id]['name']}}  </label>
                                    <input type="text" disabled="disabled" id="home_goals" name="homegoal" class="score"
                                           value="{{$results[$match->id][1]->home_goals}}">
                                    <span class="result-line"> - </span>
                                    <input type="text" disabled="disabled" id="away_goal" name="awaygoal" class="score"
                                           value="{{$results[$match->id][1]->away_goals}}">
                                    <label for="awaygoal">  {{$clubs[$match->away_club_id]['name']}} </label>
                                </span>
                                        {{ csrf_field() }}
                                        <input type="submit" disabled="disabled"
                                               class="btn-info button btn send-result-btn  btn-warning"
                                               value="Függőben">
                                    </form>
                                @elseif( isset($match->home_club_id) && isset($match->away_club_id) )
                                    <form class="result-form" action="/save-result" method="get">
                                <span class="result-inside-holder">
                                    <span class="title3 matchnr"> Első meccs </span>
                                    <label for="homegoal"> {{$clubs[$match->home_club_id]['name']}}  </label>
                                    <input type="text" id="home_goals" name="homegoal" class="score">
                                    <span class="result-line"> - </span>
                                    <input type="text" id="away_goal" name="awaygoal" class="score">
                                    <label for="awaygoal">  {{$clubs[$match->away_club_id]['name']}} </label>
                                </span>
                                        <input type="hidden" value="1" name="match_nr">
                                        <input type="hidden" value="{{$match->home_club_id}}" name="home_club_id">
                                        <input type="hidden" value="{{$match->away_club_id}}" name="away_club_id">
                                        <input type="hidden" value="{{$match->id}}" name="match_id">
                                        <input type="hidden" value="{{$champ->id}}" name="champ_id">
                                        {{ csrf_field() }}
                                        <input type="submit" class="btn-info button btn send-result-btn" value="Küldés">
                                    </form>
                                @endif
                            @endif
                            @if(!$match->isPlayedSecond)
                                @if( (isset($results[$match->id][2]) && ($results[$match->id][2]->match_nr == 2)) || (isset($results[$match->id][2]) && ($results[$match->id][2]->match_nr == 2)) )
                                    <form class="result-form" action="/save-result" method="get">
                                <span class="result-inside-holder">
                                    <span class="title3 matchnr"> Második meccs </span>
                                    <label for="awaygoal"> {{$clubs[$match->away_club_id]['name']}}  </label>
                                    <input type="text" disabled="disabled" id="away_goal" name="awaygoal" class="score"
                                           value="{{$results[$match->id][2]->away_goals}}">
                                    <span class="result-line"> - </span>
                                    <input type="text" disabled="disabled" id="home_goal" name="homegoal" class="score"
                                           value="{{$results[$match->id][2]->home_goals}}">
                                    <label for="homegoal">  {{$clubs[$match->home_club_id]['name']}} </label>
                                </span>
                                        {{ csrf_field() }}
                                        <input type="submit" disabled="disabled"
                                               class="btn-info button btn send-result-btn  btn-warning"
                                               value="Függőben">
                                    </form>
                                @elseif( isset($match->home_club_id) && isset($match->away_club_id) )
                                    <form class="result-form" action="/save-result" method="get">
                                <span class="result-inside-holder">
                                    <span class="title3 matchnr"> Második meccs </span>
                                    <label for="awaygoal">  {{$clubs[$match->away_club_id]['name']}} </label>
                                    <input type="text" id="away_goal" name="awaygoal" class="score">
                                    <span class="result-line"> - </span>
                                    <input type="text" id="home_goals" name="homegoal" class="score">
                                    <label for="homegoal"> {{$clubs[$match->home_club_id]['name']}}  </label>
                                    <input type="hidden" value="2" name="match_nr">
                                </span>

                                        <input type="hidden" value="2" name="match_nr">
                                        <input type="hidden" value="{{$match->home_club_id}}" name="home_club_id">
                                        <input type="hidden" value="{{$match->away_club_id}}" name="away_club_id">
                                        <input type="hidden" value="{{$match->id}}" name="match_id">
                                        <input type="hidden" value="{{$champ->id}}" name="champ_id">
                                        {{ csrf_field() }}
                                        <input type="submit" class="btn-info button btn send-result-btn" value="Küldés">
                                    </form>
                                @endif
                            @endif
                        @else
                            @if(!$match->isPlayedFirst)
                                @if( (isset($results[$match->id][1]) && ($results[$match->id][1]->match_nr == 1)) || (isset($results[$match->id][1]) && ($results[$match->id][1]->match_nr == 1)) )
                                    <form class="result-form" action="/save-result" method="get">
                                <span class="result-inside-holder">
                                    <label for="homegoal"> {{$clubs[$match->home_club_id]['name']}}  </label>
                                    <input type="text" disabled="disabled" id="home_goals" name="homegoal" class="score"
                                           value="{{$results[$match->id][1]->home_goals}}">
                                    <span class="result-line"> - </span>
                                    <input type="text" disabled="disabled" id="away_goal" name="awaygoal" class="score"
                                           value="{{$results[$match->id][1]->home_goals}}">
                                    <label for="awaygoal">  {{$clubs[$match->away_club_id]['name']}} </label>
                                </span>
                                        <input type="submit" disabled="disabled"
                                               class="btn-info button btn send-result-btn  btn-warning"
                                               value="Függőben">
                                    </form>
                                @elseif( isset($match->home_club_id) && isset($match->away_club_id) )
                                    <form class="result-form" action="/save-result" method="get">
                            <span class="result-inside-holder">
                                <label for="homegoal"> {{$clubs[$match->home_club_id]['name']}}  </label>
                                <input type="text" id="home_goals" name="homegoal" class="score">
                                <span class="result-line"> - </span>
                                <input type="text" id="away_goal" name="awaygoal" class="score">
                                <label for="awaygoal">  {{$clubs[$match->away_club_id]['name']}} </label>
                            </span>
                                        <input type="hidden" value="1" name="match_nr">
                                        <input type="hidden" value="{{$match->home_club_id}}" name="home_club_id">
                                        <input type="hidden" value="{{$match->away_club_id}}" name="away_club_id">
                                        <input type="hidden" value="{{$match->id}}" name="match_id">
                                        <input type="hidden" value="{{$champ->id}}" name="champ_id">
                                        {{ csrf_field() }}
                                        <input type="submit" class="btn-info button btn" value="Küldés">
                                    </form>
                                @endif
                            @endif
                        @endif
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@endsection

<script>
    document.addEventListener("DOMContentLoaded", function () {
        $(".result-form").on("submit", function () {
            return confirm("Biztos vagy benne?");
        });
    });
</script>