<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Jelszó visszaállítás sikeres!',
    'sent' => 'Elküldtük email-ben a jelszó visszaállításához szükséges linket!',
    'throttled' => 'Kérlek várj.',
    'token' => 'Érvénytelen token.',
    'user' => "Nem található felhasználó ezzel az email címmel.",

];
