<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Championship extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'championship';

}
