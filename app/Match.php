<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'match';

    protected $fillable = ['champ_id', 'home_club_id', 'away_club_id', 'hasRematch', 'timestamp', 'type', 'bracket_id'];

}
