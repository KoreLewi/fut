<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'club';

    protected $fillable = ['name', 'user_id', 'champ_id', 'played', 'won', 'draw', 'lost', 'point', 'goal_for', 'goal_against', 'goal_difference'];

}
