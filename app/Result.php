<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'results';

    protected $fillable = ['match_id','champ_id', 'home_club_id', 'away_club_id', 'home_goals', 'away_goals', 'winner_club_id', 'initiator_user_id', 'pending_user_id', 'status', 'match_nr'];

}
