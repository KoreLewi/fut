<?php

namespace App\Http\Controllers;

use App\Championship;
use App\Club;
use App\Result;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function myAccount()
    {
        if (\Auth::check()) {
            $user = auth()->user();

            $userClubs = Club::where('user_id', $user->id)
                ->get()->toArray();

            $participantinChamps = array();
            foreach ($userClubs as $userClub) {
                $participantinChamps[] = $userClub['champ_id'];
            }
            $participantinChamps = array_unique($participantinChamps);

            $clubsInChamp = array();
            $resultsPerChamp = array();
            foreach ($participantinChamps as $participantinChamp) {
                $clubs = Club::where('champ_id', $participantinChamp)
                    ->get()->toArray();
                $modifiedClubs = array();
                foreach ($clubs as $club) {
                    $modifiedClubs[$club['id']] = $club;
                }
                $clubsInChamp[$participantinChamp] = $modifiedClubs;

                $results = Result::where('pending_user_id', $user->id)
                    ->where('status', 0)
                    ->where('champ_id', $participantinChamp)
                    ->get()->toArray();
                $resultsPerChamp[$participantinChamp] = $results;
            }

            $champs = Championship::whereIn('id', $participantinChamps)
                ->get()->toArray();
            $modifiedChamps = array();
            foreach ($champs as $champ) {
                $modifiedChamps[$champ['id']] = $champ;
            }

            return view('layouts.myaccount')->with([
                'user' => $user,
                'participantinChamps' => $participantinChamps,
                'clubsInChamp' => $clubsInChamp,
                'results' => $resultsPerChamp,
                'champs' => $modifiedChamps
            ]);
        }
        return redirect()->back();
    }
}
