<?php

namespace App\Http\Controllers;

use App\Championship;
use App\Club;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MatchController extends Controller
{
    public function show($champId)
    {
        if (\Auth::check()) {
            $user = auth()->user();
            $champ = Championship::find($champId);
            $club = DB::table('club')
                ->where('champ_id', $champId)
                ->where('user_id', $user->id)
                ->first();

            if (!$club) {
                return view('champ.result')->with([
                    'user' => $user,
                    'error' => 'error'
                ]);

            }
            $clubs = Club::where('champ_id', $champId)
                ->get()->toArray();
            $modifiedClubs = array();
            foreach ($clubs as $clubb) {
                $modifiedClubs[$clubb['id']] = $clubb;
            }

            $matches = DB::table('match')
                ->where('home_club_id', $club->id)
                ->orWhere('away_club_id', $club->id)
                ->where('winner_club_id', NULL)
                ->get();
            $results = array();
            foreach ($matches as $match) {
                $result = Db::table('results')
                    ->where('match_id', $match->id)
                    ->where('status', '0')
                    ->get()
                    ->toArray();
                if ($result) {
                    if (sizeof($result) > 1) {
                        $temp = [];
                        foreach ($result as $res) {
                            if ($res->match_nr == 1)
                                $temp[1] = $res;
                            elseif ($res->match_nr == 2)
                                $temp[2] = $res;
                        }
                        $results[$match->id] = $temp;
                    } else {
                        if ($result[0]->match_nr == 1) {
                            $temp[1] = $result[0];
                        } elseif ($result[0]->match_nr == 2) {
                            $temp[2] = $result[0];
                        }
                        $results[$match->id] = $temp;
                    }
                }
            }

            return view('champ.result')->with([
                'user' => $user,
                'matches' => $matches,
                'champ' => $champ,
                'clubs' => $modifiedClubs,
                'results' => $results
            ]);
        } else {
            return redirect('/');
        }
    }
}
