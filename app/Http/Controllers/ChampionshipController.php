<?php

namespace App\Http\Controllers;

use App\Club;
use App\Match;
use Illuminate\Http\Request;
use App\Championship;
use Mockery\Exception;
use Carbon\Carbon;


class ChampionshipController extends Controller
{
    public function show(Request $request, $id)
    {
//        $this->generateTournament($id);
        $champ = Championship::find($id);
        $clubs = Club::where('champ_id', $id)
            ->get()->toArray();
        $modifiedClubs = array();
        foreach ($clubs as $club) {
            $modifiedClubs[$club['id']] = $club;
        }
        $matches = Match::where('champ_id', $id)
            ->get()->toArray();

        $modifiedMatches = array();
        foreach ($matches as $match) {
            if ($match['type'] == 'SecondRound')
                $modifiedMatches['SecondRound'][] = $match;
            elseif ($match['type'] == 'FirstRound')
                $modifiedMatches['FirstRound'][] = $match;
            elseif ($match['type'] == 'RoundOf16')
                $modifiedMatches['RoundOf16'][] = $match;
            elseif ($match['type'] == 'Quarterfinals')
                $modifiedMatches['Quarterfinals'][] = $match;
            elseif ($match['type'] == 'Semifinals')
                $modifiedMatches['Semifinals'][] = $match;
            elseif ($match['type'] == 'Finals')
                $modifiedMatches['Finals'][] = $match;
        }

        $template = '';
        $depth = '';
        if ((sizeof($clubs) >= 2) && (sizeof($clubs) <= 8)) {
            $template = 'champ.quarterfinals';
            $depth = 'QuarterFinals';
        } elseif ((sizeof($clubs) > 8) && (sizeof($clubs) <= 16)) {
            $template = 'champ.round_of_16';
            $depth = 'RoundOf16';
        } elseif ((sizeof($clubs) > 16) && (sizeof($clubs) <= 32)) {
            $template = 'champ.first_round';
            $depth = 'FirstRound';
        } elseif ((sizeof($clubs) > 32) && (sizeof($clubs) <= 64)) {
            $template = 'champ.second_round';
            $depth = 'SecondRound';
        }

        if (is_null($champ)) {
            return redirect('/');
        } else {
            return view($template)->with([
                'champ' => $champ,
                'matches' => $modifiedMatches,
                'clubs' => $modifiedClubs
            ]);
        }
    }

    public function generateTournament($id)
    {
        $clubs = Club::where('champ_id', $id)
            ->inRandomOrder()
            ->get()->toArray();

        $depth = '';
        if ((sizeof($clubs) >= 2) && (sizeof($clubs) <= 8)) {
            $depth = 'QuarterFinals';
        } elseif ((sizeof($clubs) > 8) && (sizeof($clubs) <= 16)) {
            $depth = 'RoundOf16';
        } elseif ((sizeof($clubs) > 16) && (sizeof($clubs) <= 32)) {
            $depth = 'FirstRound';
        } elseif ((sizeof($clubs) > 32) && (sizeof($clubs) <= 64)) {
            $depth = 'SecondRound';
        }

        $bracketIdentifiers = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'BB', 'CC', 'DD', 'EE', 'FF', 'GG', 'HH', 'II', 'JJ', 'KK', 'LL', 'MM', 'NN', 'OO', 'PP', 'RR', 'SS', 'TT', 'UU', 'VV', 'WW', 'XX', 'YY', 'ZZ'];
        $j = 1;

        try {
            if ($depth == 'QuarterFinals') {
                //Create Quarterfinals matches
                for ($i = 0; $i < sizeof($clubs); $i += 2) {
                    $timestamp = Carbon::now()->timestamp;
                    $firstClub = $clubs[$i];
                    $secondClub = $clubs[$i + 1];
                    Match::create([
                        'champ_id' => $id,
                        'home_club_id' => $firstClub['id'],
                        'away_club_id' => $secondClub['id'],
                        'hasRematch' => true,
                        'timestamp' => $timestamp . $bracketIdentifiers[$j],
                        'type' => 'Quarterfinals',
//                    'bracket_id' => $bracketIdentifiers[$j] . '1/' . $bracketIdentifiers[$j] . '2',
                        'bracket_id' => 'Q#' . $j,
                    ]);
                    $j++;
                }

                //Create Semifinals
                Match::create([
                    'champ_id' => $id,
                    'hasRematch' => true,
                    'timestamp' => $timestamp . $bracketIdentifiers[$j],
                    'type' => 'Semifinals',
                    'bracket_id' => 'SF#1'
                ]);
                Match::create([
                    'champ_id' => $id,
                    'hasRematch' => true,
                    'timestamp' => $timestamp . $bracketIdentifiers[$j],
                    'type' => 'Semifinals',
                    'bracket_id' => 'SF#2'
                ]);

                //Create Final
                Match::create([
                    'champ_id' => $id,
                    'hasRematch' => false,
                    'isFinal' => true,
                    'timestamp' => $timestamp . $bracketIdentifiers[$j],
                    'type' => 'Finals',
                    'bracket_id' => 'FINAL#' . $id,
                ]);
            } elseif ($depth == 'RoundOf16') {

                //Create Additional clubs
                if ((16 - sizeof($clubs)) != 0) {
                    $plusClubs = 16 - sizeof($clubs);
                    for ($i = 1; $i <= $plusClubs; $i++) {
                        Club::create([
                            'name' => 'AdditionalClub',
                            'user_id' => 1,
                            'champ_id' => $id,
                            'played' => '0',
                            'won' => '0',
                            'draw' => '0',
                            'lost' => '0',
                            'point' => '0',
                            'goal_for' => '0',
                            'goal_against' => '0',
                            'goal_difference' => '0'
                        ]);
                    }
                }

                //Create RoundOf16 matches - 16 club
                for ($i = 1; $i <= 8; $i++) {
                    $timestamp = Carbon::now()->timestamp;
                    Match::create([
                        'champ_id' => $id,
                        'hasRematch' => true,
                        'timestamp' => $timestamp . $bracketIdentifiers[$i],
                        'type' => 'RoundOf16',
                        'bracket_id' => 'R16#' . $i,
                    ]);
                }
                //Create Quarterfinal matches - 8 club
                for ($i = 1; $i <= 4; $i++) {
                    $timestamp = Carbon::now()->timestamp;
                    Match::create([
                        'champ_id' => $id,
                        'hasRematch' => true,
                        'timestamp' => $timestamp . $bracketIdentifiers[$i],
                        'type' => 'Quarterfinals',
                        'bracket_id' => 'Q#' . $i,
                    ]);
                }

                //Create SemiFinals
                for ($i = 1; $i <= 2; $i++) {
                    $timestamp = Carbon::now()->timestamp;
                    Match::create([
                        'champ_id' => $id,
                        'hasRematch' => true,
                        'timestamp' => $timestamp . $bracketIdentifiers[$i],
                        'type' => 'Semifinals',
                        'bracket_id' => 'SF#' . $i,
                    ]);
                }

                //Create Final
                $timestamp = Carbon::now()->timestamp;
                Match::create([
                    'champ_id' => $id,
                    'hasRematch' => false,
                    'isFinal' => true,
                    'timestamp' => $timestamp . $bracketIdentifiers[$j],
                    'type' => 'Finals',
                    'bracket_id' => 'FINAL#' . $id,
                ]);

            } elseif ($depth == 'FirstRound') {

                //Create Additional clubs
                if ((32 - sizeof($clubs)) != 0) {
                    $plusClubs = 32 - sizeof($clubs);
                    for ($i = 1; $i <= $plusClubs; $i++) {
                        Club::create([
                            'name' => 'AdditionalClub',
                            'user_id' => 1,
                            'champ_id' => $id,
                            'played' => '0',
                            'won' => '0',
                            'draw' => '0',
                            'lost' => '0',
                            'point' => '0',
                            'goal_for' => '0',
                            'goal_against' => '0',
                            'goal_difference' => '0'
                        ]);
                    }
                }

                //Create FirstRound matches - 32 club
                for ($i = 0; $i < sizeof($clubs); $i += 2) {
                    $timestamp = Carbon::now()->timestamp;
                    $firstClub = $clubs[$i];
                    $secondClub = $clubs[$i + 1];
                    Match::create([
                        'champ_id' => $id,
                        'home_club_id' => $firstClub['id'],
                        'away_club_id' => $secondClub['id'],
                        'hasRematch' => true,
                        'timestamp' => $timestamp . $bracketIdentifiers[$j],
                        'type' => 'FirstRound',
                        'bracket_id' => 'FirR#' . $j,
                    ]);
                    $j++;
                }

                //Create RoundOf16 matches - 16 club
                for ($i = 1; $i <= 8; $i++) {
                    $timestamp = Carbon::now()->timestamp;
                    Match::create([
                        'champ_id' => $id,
                        'hasRematch' => true,
                        'timestamp' => $timestamp . $bracketIdentifiers[$i],
                        'type' => 'RoundOf16',
                        'bracket_id' => 'R16#' . $i,
                    ]);
                }
                //Create Quarterfinal matches - 8 club
                for ($i = 1; $i <= 4; $i++) {
                    $timestamp = Carbon::now()->timestamp;
                    Match::create([
                        'champ_id' => $id,
                        'hasRematch' => true,
                        'timestamp' => $timestamp . $bracketIdentifiers[$i],
                        'type' => 'Quarterfinals',
                        'bracket_id' => 'Q#' . $i,
                    ]);
                }

                //Create SemiFinals
                for ($i = 1; $i <= 2; $i++) {
                    $timestamp = Carbon::now()->timestamp;
                    Match::create([
                        'champ_id' => $id,
                        'hasRematch' => true,
                        'timestamp' => $timestamp . $bracketIdentifiers[$i],
                        'type' => 'Semifinals',
                        'bracket_id' => 'SF#' . $i,
                    ]);
                }

                //Create Final
                $timestamp = Carbon::now()->timestamp;
                Match::create([
                    'champ_id' => $id,
                    'hasRematch' => false,
                    'isFinal' => true,
                    'timestamp' => $timestamp . $bracketIdentifiers[$j],
                    'type' => 'Finals',
                    'bracket_id' => 'FINAL#' . $id,
                ]);


            } elseif ($depth == 'SecondRound') {

                //Create Additional clubs
                if ((64 - sizeof($clubs)) != 0) {
                    $plusClubs = 64 - sizeof($clubs);
                    for ($i = 1; $i <= $plusClubs; $i++) {
                        Club::create([
                            'name' => 'AdditionalClub',
                            'user_id' => 1,
                            'champ_id' => $id,
                            'played' => '0',
                            'won' => '0',
                            'draw' => '0',
                            'lost' => '0',
                            'point' => '0',
                            'goal_for' => '0',
                            'goal_against' => '0',
                            'goal_difference' => '0'
                        ]);
                    }
                }

                //Create SecondRound matches - 64 club
                for ($i = 0; $i < sizeof($clubs); $i += 2) {
                    $timestamp = Carbon::now()->timestamp;
                    $firstClub = $clubs[$i];
                    $secondClub = $clubs[$i + 1];
                    Match::create([
                        'champ_id' => $id,
                        'home_club_id' => $firstClub['id'],
                        'away_club_id' => $secondClub['id'],
                        'hasRematch' => true,
                        'timestamp' => $timestamp . $bracketIdentifiers[$j],
                        'type' => 'SecondRound',
                        'bracket_id' => 'SecR#' . $j,
                    ]);
                    $j++;
                }

                //Create FirstRound matches - 32 club
                for ($i = 1; $i <= 16; $i++) {
                    $timestamp = Carbon::now()->timestamp;
                    Match::create([
                        'champ_id' => $id,
                        'hasRematch' => true,
                        'timestamp' => $timestamp . $bracketIdentifiers[$i],
                        'type' => 'FirstRound',
                        'bracket_id' => 'FirR#' . $i,
                    ]);
                }

                //Create RoundOf16 matches - 16 club
                for ($i = 1; $i <= 8; $i++) {
                    $timestamp = Carbon::now()->timestamp;
                    Match::create([
                        'champ_id' => $id,
                        'hasRematch' => true,
                        'timestamp' => $timestamp . $bracketIdentifiers[$i],
                        'type' => 'RoundOf16',
                        'bracket_id' => 'R16#' . $i,
                    ]);
                }
                //Create Quarterfinal matches - 8 club
                for ($i = 1; $i <= 4; $i++) {
                    $timestamp = Carbon::now()->timestamp;
                    Match::create([
                        'champ_id' => $id,
                        'hasRematch' => true,
                        'timestamp' => $timestamp . $bracketIdentifiers[$i],
                        'type' => 'Quarterfinals',
                        'bracket_id' => 'Q#' . $i,
                    ]);
                }

                //Create SemiFinals
                for ($i = 1; $i <= 2; $i++) {
                    $timestamp = Carbon::now()->timestamp;
                    Match::create([
                        'champ_id' => $id,
                        'hasRematch' => true,
                        'timestamp' => $timestamp . $bracketIdentifiers[$i],
                        'type' => 'Semifinals',
                        'bracket_id' => 'SF#' . $i,
                    ]);
                }

                //Create Final
                $timestamp = Carbon::now()->timestamp;
                Match::create([
                    'champ_id' => $id,
                    'hasRematch' => false,
                    'isFinal' => true,
                    'timestamp' => $timestamp . $bracketIdentifiers[$j],
                    'type' => 'Finals',
                    'bracket_id' => 'FINAL#' . $id,
                ]);
            }

            $champ = Championship::find($id);
            $champ->status = 'In progress';
            $champ->save();

        } catch (Exception $e) {
            echo 'A problem has occured';
        }
        return true;
    }

    public function registerShow($id)
    {
        if (\Auth::check()) {
            $user = auth()->user();
        }else{
            return view('champ.register')->with([
                'error' => 'A jelentkezéshez regisztráció szükséges.'
            ]);
        }
        $alreadyRegistered = Club::where('champ_id', $id)
            ->where('user_id', $user->id)
            ->get()->toArray();
        if ($alreadyRegistered) {
            $champ = Championship::where('id', $id)
                ->first();

            return view('champ.register')->with([
                'error_already' => 'Már jelentkeztél erre a bajnokságra',
                'champ' => $champ
            ]);
        } else {
            $champ = Championship::where('id', $id)
                ->first();

            return view('champ.register')->with([
                'champ' => $champ
            ]);
        }
        return redirect()->back();
    }

    public function register(Request $request)
    {
        if (\Auth::check()) {
            $user = auth()->user();

            $champId = $request->input('champ_id');

            $champ = Championship::find($champId);
            if($champ->status != 'Registration'){
                return redirect()->back()->with('error', 'A jelentkezés erre a bajnokságra lezárult.');
            }

            $alreadyRegistered = Club::where('champ_id', $champId)
                ->where('user_id', $user->id)
                ->get()->toArray();

            if ($alreadyRegistered) {
                return redirect()->back();
            } else {
                Club::create([
                    'name' => $user->name,
                    'user_id' => $user->id,
                    'champ_id' => $champId,
                    'played' => '0',
                    'won' => '0',
                    'draw' => '0',
                    'lost' => '0',
                    'point' => '0',
                    'goal_for' => '0',
                    'goal_against' => '0',
                    'goal_difference' => '0'
                ]);
            }

            return redirect('/my-account');
        }
        return redirect()->back();
    }
}
