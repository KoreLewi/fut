<?php

namespace App\Http\Controllers;

use App\Championship;
use App\Club;
use App\Http\Middleware\Authenticate;
use App\Match;
use App\Result;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Mockery\Exception;

class ResultController extends Controller
{
    public function save(Request $request)
    {
        if (\Auth::check()) {
            $user = auth()->user();
            $matchId = $request->input('match_id');
            $homeGoals = $request->input('homegoal');
            $awayGoals = $request->input('awaygoal');
            $homeClubId = $request->input('home_club_id');
            $awayClubId = $request->input('away_club_id');
            $matcNr = $request->input('match_nr');
            $champId = $request->input('champ_id');

            $homeClub = Club::where('id', $homeClubId)->get();
            $awayClub = Club::where('id', $awayClubId)->get();
            $homeClub = $homeClub[0];
            $awayClub = $awayClub[0];
            if ($homeClub->user_id == $user->id) {
                $pendingUserId = $awayClub->user_id;
            } elseif ($awayClub->user_id == $user->id) {
                $pendingUserId = $homeClub->user_id;
            }

            $winnerClubId = '';
            if ($homeGoals > $awayGoals) {
                $winnerClubId = $homeClubId;
            } elseif ($homeGoals < $awayGoals) {
                $winnerClubId = $awayClubId;
            } elseif ($homeGoals == $awayGoals) {
                $winnerClubId = '000000';
            }

            Result::create([
                'match_id' => $matchId,
                'champ_id' => $champId,
                'home_club_id' => $homeClubId,
                'away_club_id' => $awayClubId,
                'home_goals' => $homeGoals,
                'away_goals' => $awayGoals,
                'winner_club_id' => $winnerClubId,
                'initiator_user_id' => $user->id,
                'pending_user_id' => $pendingUserId,
                'status' => '0',
                'match_nr' => $matcNr
            ]);
            return redirect()->back()->with('status', 'Sikeres mentés.');
        }

    }

    public function acceptResult(Request $request)
    {
        if (\Auth::check()) {
            $user = auth()->user();
            $resultId = $request->input('result_id');

            $result = Result::where('id', $resultId)
                ->first()->toArray();

            $match = Match::where('id', $result['match_id'])
                ->first();

            $champId = $match->champ_id;
            try {
                if ($result['home_club_id'] != $match->home_club_id) {
                    $helper = $result['home_goals'];
                    $result['home_goals'] = $result['away_goals'];
                    $result['away_goals'] = $helper;
                }
                if ($match->hasRematch) {
                    if (!$match->isPlayedFirst) {
                        $match->first_match_home_goals = $result['home_goals'];
                        $match->first_match_away_goals = $result['away_goals'];
                        $match->total_home_goals = $match->total_home_goals + $result['home_goals'];
                        $match->total_away_goals = $match->total_away_goals + $result['away_goals'];
                        $match->isPlayedFirst = true;
                        $match->save();
                    } elseif ($match->isPlayedFirst && !$match->isPlayedSecond) {
                        $totalHomeGoals = $result['home_goals'] + $match->first_match_home_goals;
                        $totalAwayGoals = $result['away_goals'] + $match->first_match_away_goals;

                        if ($totalHomeGoals == $totalAwayGoals) {
                            return redirect()->back()->with(['msg', 'A két meccs összeredménye nem lehet egyenlő! Olvasd el a szabályztatot.']);
                        } else {
                            if ($totalHomeGoals > $totalAwayGoals) {
                                $winnerClubId = $match->home_club_id;
                            } elseif ($totalHomeGoals < $totalAwayGoals) {
                                $winnerClubId = $match->away_club_id;
                            }
                        }
                        $match->second_match_home_goals = $result['home_goals'];
                        $match->second_match_away_goals = $result['away_goals'];
                        $match->total_home_goals = $match->total_home_goals + $result['home_goals'];
                        $match->total_away_goals = $match->total_away_goals + $result['away_goals'];
                        $match->winner_club_id = $winnerClubId;
                        $match->isPlayedSecond = true;
                        $match->save();

                        switch ($match->bracket_id){
                            case 'SecR#1':
                                $nextBracket = 'FirR#1';
                                break;
                            case 'SecR#2':
                                $nextBracket = 'FirR#1';
                                break;
                            case 'SecR#3':
                                $nextBracket = 'FirR#2';
                                break;
                            case 'SecR#4':
                                $nextBracket = 'FirR#2';
                                break;
                            case 'SecR#5':
                                $nextBracket = 'FirR#3';
                                break;
                            case 'SecR#6':
                                $nextBracket = 'FirR#3';
                                break;
                            case 'SecR#7':
                                $nextBracket = 'FirR#4';
                                break;
                            case 'SecR#8':
                                $nextBracket = 'FirR#4';
                                break;
                            case 'SecR#9':
                                $nextBracket = 'FirR#5';
                                break;
                            case 'SecR#10':
                                $nextBracket = 'FirR#5';
                                break;
                            case 'SecR#11':
                                $nextBracket = 'FirR#6';
                                break;
                            case 'SecR#12':
                                $nextBracket = 'FirR#6';
                                break;
                            case 'SecR#13':
                                $nextBracket = 'FirR#7';
                                break;
                            case 'SecR#14':
                                $nextBracket = 'FirR#7';
                                break;
                            case 'SecR#15':
                                $nextBracket = 'FirR#8';
                                break;
                            case 'SecR#16':
                                $nextBracket = 'FirR#8';
                                break;
                            case 'SecR#17':
                                $nextBracket = 'FirR#9';
                                break;
                            case 'SecR#18':
                                $nextBracket = 'FirR#9';
                                break;
                            case 'SecR#19':
                                $nextBracket = 'FirR#10';
                                break;
                            case 'SecR#20':
                                $nextBracket = 'FirR#10';
                                break;
                            case 'SecR#21':
                                $nextBracket = 'FirR#11';
                                break;
                            case 'SecR#22':
                                $nextBracket = 'FirR#11';
                                break;
                            case 'SecR#23':
                                $nextBracket = 'FirR#12';
                                break;
                            case 'SecR#24':
                                $nextBracket = 'FirR#12';
                                break;
                            case 'SecR#25':
                                $nextBracket = 'FirR#13';
                                break;
                            case 'SecR#26':
                                $nextBracket = 'FirR#13';
                                break;
                            case 'SecR#27':
                                $nextBracket = 'FirR#14';
                                break;
                            case 'SecR#28':
                                $nextBracket = 'FirR#14';
                                break;
                            case 'SecR#29':
                                $nextBracket = 'FirR#15';
                                break;
                            case 'SecR#30':
                                $nextBracket = 'FirR#15';
                                break;
                            case 'SecR#31':
                                $nextBracket = 'FirR#16';
                                break;
                            case 'SecR#32':
                                $nextBracket = 'FirR#16';
                                break;
                            case 'SecR#33':
                                $nextBracket = 'FirR#17';
                                break;
                            case 'SecR#34':
                                $nextBracket = 'FirR#17';
                                break;
                            case 'SecR#35':
                                $nextBracket = 'FirR#18';
                                break;
                            case 'SecR#36':
                                $nextBracket = 'FirR#18';
                                break;
                            case 'SecR#37':
                                $nextBracket = 'FirR#19';
                                break;
                            case 'SecR#38':
                                $nextBracket = 'FirR#19';
                                break;
                            case 'SecR#39':
                                $nextBracket = 'FirR#20';
                                break;
                            case 'SecR#40':
                                $nextBracket = 'FirR#20';
                                break;
                            case 'SecR#41':
                                $nextBracket = 'FirR#21';
                                break;
                            case 'SecR#42':
                                $nextBracket = 'FirR#21';
                                break;
                            case 'SecR#43':
                                $nextBracket = 'FirR#22';
                                break;
                            case 'SecR#44':
                                $nextBracket = 'FirR#22';
                                break;
                            case 'SecR#45':
                                $nextBracket = 'FirR#23';
                                break;
                            case 'SecR#46':
                                $nextBracket = 'FirR#23';
                                break;
                            case 'SecR#47':
                                $nextBracket = 'FirR#24';
                                break;
                            case 'SecR#48':
                                $nextBracket = 'FirR#24';
                                break;
                            case 'SecR#49':
                                $nextBracket = 'FirR#25';
                                break;
                            case 'SecR#50':
                                $nextBracket = 'FirR#25';
                                break;
                            case 'SecR#51':
                                $nextBracket = 'FirR#26';
                                break;
                            case 'SecR#52':
                                $nextBracket = 'FirR#26';
                                break;
                            case 'SecR#53':
                                $nextBracket = 'FirR#27';
                                break;
                            case 'SecR#54':
                                $nextBracket = 'FirR#27';
                                break;
                            case 'SecR#55':
                                $nextBracket = 'FirR#28';
                                break;
                            case 'SecR#56':
                                $nextBracket = 'FirR#28';
                                break;
                            case 'SecR#57':
                                $nextBracket = 'FirR#29';
                                break;
                            case 'SecR#58':
                                $nextBracket = 'FirR#29';
                                break;
                            case 'SecR#59':
                                $nextBracket = 'FirR#30';
                                break;
                            case 'SecR#60':
                                $nextBracket = 'FirR#30';
                                break;
                            case 'SecR#61':
                                $nextBracket = 'FirR#31';
                                break;
                            case 'SecR#62':
                                $nextBracket = 'FirR#31';
                                break;
                            case 'SecR#63':
                                $nextBracket = 'FirR#32';
                                break;
                            case 'SecR#64':
                                $nextBracket = 'FirR#32';
                                break;
                            case 'FirR#1':
                                $nextBracket = 'R16#1';
                                break;
                            case 'FirR#2':
                                $nextBracket = 'R16#1';
                                break;
                            case 'FirR#3':
                                $nextBracket = 'R16#2';
                                break;
                            case 'FirR#4':
                                $nextBracket = 'R16#2';
                                break;
                            case 'FirR#5':
                                $nextBracket = 'R16#3';
                                break;
                            case 'FirR#6':
                                $nextBracket = 'R16#3';
                                break;
                            case 'FirR#7':
                                $nextBracket = 'R16#4';
                                break;
                            case 'FirR#8':
                                $nextBracket = 'R16#4';
                                break;
                            case 'FirR#9':
                                $nextBracket = 'R16#5';
                                break;
                            case 'FirR#10':
                                $nextBracket = 'R16#5';
                                break;
                            case 'FirR#11':
                                $nextBracket = 'R16#6';
                                break;
                            case 'FirR#12':
                                $nextBracket = 'R16#6';
                                break;
                            case 'FirR#13':
                                $nextBracket = 'R16#7';
                                break;
                            case 'FirR#14':
                                $nextBracket = 'R16#7';
                                break;
                            case 'FirR#15':
                                $nextBracket = 'R16#8';
                                break;
                            case 'FirR#16':
                                $nextBracket = 'R16#8';
                                break;
                            case 'R16#1':
                                $nextBracket = 'Q#1';
                                break;
                            case 'R16#2':
                                $nextBracket = 'Q#1';
                                break;
                            case 'R16#3':
                                $nextBracket = 'Q#2';
                                break;
                            case 'R16#4':
                                $nextBracket = 'Q#2';
                                break;
                            case 'R16#5':
                                $nextBracket = 'Q#3';
                                break;
                            case 'R16#6':
                                $nextBracket = 'Q#3';
                                break;
                            case 'R16#7':
                                $nextBracket = 'Q#4';
                                break;
                            case 'R16#8':
                                $nextBracket = 'Q#4';
                                break;
                            case 'Q#1':
                                $nextBracket = 'SF#1';
                                break;
                            case 'Q#2':
                                $nextBracket = 'SF#1';
                                break;
                            case 'Q#3':
                                $nextBracket = 'SF#2';
                                break;
                            case 'Q#4':
                                $nextBracket = 'SF#2';
                                break;
                            case 'SF#1':
                                $nextBracket = 'FINAL';
                                break;
                            case 'SF#2':
                                $nextBracket = 'FINAL';
                                break;
                        }

                        $nextMatch = Match::where('champ_id', $champId)
                                ->where('bracket_id', $nextBracket)
                                ->first();
                        $bracketIdentificator = explode('#',$match->bracket_id);
                        $bracketIdentificator = $bracketIdentificator[1];
                        if($bracketIdentificator % 2 == 0){
                            $nextMatch->away_club_id = $winnerClubId;
                        }else{
                            $nextMatch->home_club_id = $winnerClubId;
                        }
//                        if(!isset($nextMatch->home_club_id)){
//
//                        }elseif(isset($nextMatch->home_club_id) && !isset($nextMatch->away_club_id)){
//
//                        }
                        $nextMatch->save();
                    }
                } else {
                    $match->first_match_home_goals = $result['home_goals'];
                    $match->first_match_away_goals = $result['away_goals'];
                    $match->total_home_goals = $match->total_home_goals + $result['home_goals'];
                    $match->total_away_goals = $match->total_away_goals + $result['away_goals'];
                    $match->winner_club_id = $result['winner_club_id'];
                    $match->save();
                }
                $result = Result::find($resultId);
                $result->status = 1;
                $result->save();
            } catch (Exception $e) {

            }
            return redirect()->back()->with('status', 'Sikeres frissítés.');
        }
        return redirect()->back();
    }
}
