<?php

namespace App\Http\Controllers;

use App\Championship;
use App\Club;
use Illuminate\Http\Request;

class HeaderController extends Controller
{
    public static function headerMenu()
    {
        if (\Auth::check()) {
            $user = auth()->user();

            $championship = Championship::where('status', 'In progress')->where('platform', $user->platform)->first();
            $activ = Club::where('champ_id',$championship->id)->where('user_id', $user->id)->get()->toArray();
            $activChampId = false;
            if($activ){
                $activChampId = $championship->id;
            }

            return view('commons.header')->with([
                'user' => $user,
                'championship' => $championship,
                'activChampId' => $activChampId
            ]);
        }
        return view('commons.header');
    }
}
