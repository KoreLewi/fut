<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

//Route::get('/', 'HomeController@myIndexPage');
Route::get('/home', 'HomeController@myIndexPage');


Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


Route::get('tournament-registration-show-{id}', 'ChampionshipController@registerShow');
Route::get('tournament-registration', 'ChampionshipController@register');
Route::get('championship-{id}', 'ChampionshipController@show');

Route::get('my-matches-{id}', 'MatchController@show');

//Route::post('save-result', 'ResultController@save');

Route::get('save-result', 'ResultController@save');
Route::get('my-account', 'AccountController@myAccount');
Route::get('accept-result', 'ResultController@acceptResult');


/*Content*/
Route::get('rules', 'ContentController@rules');
