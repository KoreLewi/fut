<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('champ_id');
            $table->bigInteger('match_id');
            $table->bigInteger('home_club_id');
            $table->bigInteger('away_club_id');
            $table->integer('home_goals')->nullable();
            $table->integer('away_goals')->nullable();
            $table->bigInteger('winner_club_id');
            $table->bigInteger('initiator_user_id');
            $table->bigInteger('pending_user_id');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
