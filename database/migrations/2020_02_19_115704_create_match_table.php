<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('champ_id');
            $table->bigInteger('home_club_id');
            $table->bigInteger('away_club_id');
            $table->integer('first_match_home_goals')->nullable();
            $table->integer('first_match_away_goals')->nullable();
            $table->integer('second_match_home_goals')->nullable();
            $table->integer('second_match_away_goals')->nullable();
            $table->integer('total_home_goals')->nullable();
            $table->integer('total_away_goals')->nullable();
            $table->integer('winner_club_id')->nullable();
            $table->boolean('hasRematch')->nullable();
            $table->boolean('isFinal')->nullable();
            $table->timestamp('timestamp')->nullable();
            $table->enum('type', ['First round', 'Round of 16', 'Quarterfinals', 'Semifinals', 'Finals']);
            $table->string('bracket_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match');
    }
}
